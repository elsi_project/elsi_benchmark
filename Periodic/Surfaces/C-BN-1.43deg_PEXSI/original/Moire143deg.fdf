MixHamiltonian     .true.
######MonitorForcesInSCF .true.
DM.MixingWeight          0.3

SystemName      Moire BN/C
SystemLabel     moire

==================================================
==================================================
# SCF variables

DM.MixSCF1              .false.
MaxSCFIterations         1
#DM.Tolerance             5.d-3
DM.Tolerance             1.d-4
DM.NumberPulay            5

==================================================
==================================================
# General variables

SolutionMethod           diagon
#SolutionMethod           pexsi


#MPI.Nprocs.SIESTA 2048
PEXSI.np-per-pole 64
PEXSI.ordering 0
PEXSI.np-symbfact 4


ElectronicTemperature   25.0 meV
MeshCutoff             180.0 Ry
xc.functional            VDW
xc.authors               DRSLL
Use.New.Diagk           .true.

==================================================
==================================================
# PEXSI variables
PEXSI.inertia-count 1
PEXSI.inertia-counts 100
PEXSI.inertia-num-electron-tolerance 200
#PEXSI.inertia-max-iter 1
PEXSI.mu-max-iter 30
PEXSI.temperature 300.0
PEXSI.num-poles 40
PEXSI.mu  -0.30 Ry
PEXSI.muMin -2.00 Ry
PEXSI.muMax 0.00 Ry
#PEXSI.num-electron-tolerance 0.001
PEXSI.num-electron-tolerance-upper-bound 1
PEXSI.num-electron-tolerance-lower-bound 0.001
DM.NormalizationTolerance 1.0d-1  # (true_no_electrons/no_electrons) - 1.0 

==================================================
==================================================
# SPECIES AND BASIS


%Block PAO.Basis
B.vdw    3      0.92362
 n=2   0   2   E    99.49459     4.97748
     6.98773     2.98040
     1.00000     1.00000
 n=2   1   2   E   104.81130     5.87086
    10.30110     5.05514
     1.00000     1.00000
 n=2   2   1   E    84.56258     3.71079
     5.62996
     1.00000
N.vdw    3     -0.00139
 n=2   0   2   E    65.50216     4.29661
     5.64483     3.02914
     1.00000     1.00000
 n=2   1   2   E    30.54417     5.81284
     7.25855     2.85547
     1.00000     1.00000
 n=3   2   1   E    59.15335     0.14049
     3.65788
     1.00000
C.vdw    3       .35201
n=2   0   2   E    50.37145     5.22551
     5.43077     3.08484
     1.00000     1.00000
n=2   1   2   E    13.53326     6.81234
     6.83094     3.01366
     1.00000     1.00000
n=2   2   1   E   110.78225      .01065
     5.04748
     1.00000
%EndBlock PAO.Basis

==================================================
==================================================
# UNIT CELL AND ATOMIC POSITIONS

%include 1.43-3874.fdf
## needed when using z20

%block AtomicCoordinatesOrigin
 0.0     0.0     0.0
%endblock AtomicCoordinatesOrigin

#%block SuperCell
#1 0 0 
#0 1 0
#0 0 3
#%endblock SuperCell

==================================================
==================================================
# K-points

#%block Kgrid_Monkhorst_Pack
# 3   0   0   0.0
# 0   3   0   0.0
# 0   0   1   0.0
#%endblock Kgrid_Monkhorst_Pack

==================================================
==================================================
# Input variables

UseStructFile           .false.
#UseSaveData             .true.
#DM.UseSaveDM            .true.
#MD.UseSaveCG            .true.
#MD.UseSaveXV            .true.

==================================================
==================================================
# MD variables

MD.TypeOfRun              CG
#MD.FIRE.TimeStep       0.5 fs
#MD.FinalTimeStep       10  fs
MD.NumCGsteps            0
MD.MaxCGDispl            0.10 Ang
MD.MaxForceTol           0.01 eV/Ang
MD.VariableCell         .true.
%block GeometryConstraints
routine constr
%endblock GeometryConstraints

==================================================
==================================================
# Output variables

#WriteMullikenPop           1
WriteBands              .false.
#SaveRho                 .true.
SaveDeltaRho            .false.
SaveHS                  .false.
SaveElectrostaticPotential      .false.
SaveTotalPotential      .false.
SaveTotalCharge         .false.

WriteCoorXmol           .false.
#WriteCoorCerius         .true.
#WriteMDXmol             .true.
#WriteMDhistory          .true.
WriteCoorStep           .false.
WriteEigenvalues        .false.
WriteDenchar            .false.
WriteWaveFunctions      .false.
WriteCoorInitial        .false.
WriteForces             .false.
WriteHirshfeldPop       .false.
WriteVoronoiPop         .false.

==================================================
==================================================
