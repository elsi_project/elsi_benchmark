# Changelog #
## [Unreleased] ##
### Added ###
- A periodic material set containing 37 materials:
    - 19 bulk crystals
    - 1 liquid
    - 3 molecular systems
    - 4 nanosystems
    - 10 surfaces
- A non-periodic/molecule set containing 8 materials:
    - 3 clusters
    - 5 molecules
- Two scripts, GenerateBenchmarkSet.sh and GenerateMaterial.py, to automate
  creation of supercells.
