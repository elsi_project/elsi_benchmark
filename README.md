# The ELSI Performance and Scaling Benchmark Set #

This benchmark set is a collection of atomic-level geometries for use in
assessing the performance and scaling of electronic structure codes across
different architectures, libraries, and compiler suites. Geometries from
previous benchmarks of 15+ KS-DFT codes and KS solvers are included in this
benchmark set, alongside new materials included to fill gaps in the literature
benchmarks.

This benchmark set is part of the ELectronic Structure Infrastructure (ELSI)
project, a consortium of developers creating software interface libraries
providing consistent APIs for libraries which solve or circumvent the Kohn-Sham
generalized eigenvalue equation of electronic structure theory.  For more
information on ELSI, please visit the website at
<https://wordpress.elsi-interchange.org/>.

The development repo for this benchmark set is hosted on the ELSI GitLab at
<http://www.elsi-interchange.org:81/elsi-devel/PerformanceBenchmark>.

## Geometry Files ##

This benchmark set consists of a collection of geometry files modeling a diverse
set of physical systems, organized into different folders grouping similar
materials together into subsets.

Every folder for a material contains a file named `geometry.in` which provides
the atomic-level geometry in the file format used by the FHI-aims electronic
structure package (<https://aimsclub.fhi-berlin.mpg.de/>).  The FHI-aims file
format was chosen as it supports both periodic and non-periodic/molecular
geometries.  Instructions for converting the FHI-aims file format into other
file formats may be found in the "Converting Geometry Files To Other File
Formats" section of this document.

The (simplified) FHI-aims file format used in this benchmark set consists of
independent lines of text, each of which start with a keyword to identify the
element of the geometry being described:

* The `lattice_vector` keyword denotes a lattice vector of the unit cell.  It
  will be followed by the three Cartesian coordinates of the lattice vector
  (in Angstorms).
* The `atom` keyword denotes an atom in Cartesian coordinates.  It will be
  followed by the three Cartesian coordinates of the atom  (in Angstorms) and
  the species of the atom.
* The `atom_frac` keyword denotes an atom in direct coordinates.  It will be
  followed by the three direct coordinates of the atom and the species of the
  atom.

Non-periodic/molecular geometries contain only lines beginning with the `atom`
keyword.  Periodic geometries must contain three `lattice_vector` keywords, but
may freely mix and match any combination of `atom` and `atom_frac` keywords.  An
example geometry file in the FHI-aims file format for the primitive cell of
diamond Si with a lattice parameter of 5.4286 Angstroms is shown below:
```
lattice_vector    0.000000    2.714300    2.714300
lattice_vector    2.714300    0.000000    2.714300
lattice_vector    2.714300    2.714300    0.000000
atom              0.000000    0.000000    0.000000 Si
atom              1.357150    1.357150    1.357150 Si
```
When possible, we provide the geometry for the primitive cell of periodic
calculations, allowing the user maximum flexibility to make supercells via the
provided `GenerateMaterial.py` Python script or their own scripts.

For materials whose geometries were taken from a previous source and converted
into the FHI-aims file format, we additionally provide the geometry in the
original file format in the `original` sub-folder.

## Converting Geometry Files To Other File Formats ##

The FHI-aims file format used for geometry files in this benchmark set is one of
many electronic structure file formats supported by the Open Babel utility
(<http://openbabel.org/>, version 2.3.0+).  To convert a geometry file from this
benchmark set into a different format using Open Babel, one may use the
following command (tested for Open Babel 2.3.2):
```
  babel -ifhiaims geometry.in -oFORMAT FILENAME
```
where `FORMAT` is the desired file format and `FILENAME` is the desired file
name.  For more information about using Open Babel, please see the documentation
for Open Babel at <http://openbabel.org/>.

Another tool for converting geometry files into different file formats is the
Atomic Simulation Environment (ASE, <https://wiki.fysik.dtu.dk/ase/>, version
3.2.0+), "a set of tools and Python modules for setting up, manipulating,
running, visualizing and analyzing atomistic simulations".  Provided below is an
example Python script for converting a geometry file from this benchmark set
into a different format using ASE (tested for ASE 3.16.2 and Python 3.5.2):
```python3
from ase.io import read, write
geometry_file = read("geometry.in", format="aims")
write("FILENAME", geometry_file, format="FORMAT")
```
where `FORMAT` is the desired file format and `FILENAME` is the desired file
name.  For more information about using ASE, please see the documentation for
ASE at
<https://wiki.fysik.dtu.dk/ase/>.

## Metadata ##

Every geometry file contains a five-line metadata header to describe the
simulated material.  The five lines of the metadata header contain the following
information:

- Material: Brief description of the material, e.g. chemical formula, common
  name used in literature
- Structure: Details about the structure used for the geometry of the material,
  e.g. phase, space group, measurement temperature, measurement pressure
- Source: The source(s) for the geometry, e.g. individuals who provided the
  geometry to this benchmark set, citations to journal articles where results
  for the geometry were published, DOIs to entries in online materials
  repositories.
- Keywords:  Keywords describing the material, intended for use with automated
  tools while still being human-readable.  The keywords are defined in the
  "Keywords" section of this document.
- Note: Additional information which does not fit into the other metadata header
  lines.  Modifications of the geometry from the original source are noted here.

Each line of the metadata header starts with a "#" symbol.

An example metadata header is shown below:
```
# Material:  Methylammonium lead iodide (MAPI)
# Structure: Non-physical well-ordered cubic phase
# Source:    Provided by Stephan Mohr, used in Mohr et al., Phys Chem Chem Phys 17, 31360 (2015) and arXiv:1704.00512
# Keywords:  3D, Provided, Article, Bulk
# Note:      Units of original geometry were in bohr.  A conversion factor of 1 bohr = 0.52917721 Angstroms was used.
```

## Keywords ##

Each geometry file contains a set of keywords classifying the geometry in the
Keywords line of the metadata header.  In this section, we provide a
(non-exhaustive) list of keywords and their meanings.

**Dimensionality Keywords**

Many electronic structures packages and solver libraries exhibit different
scaling characteristics based on the physical dimensionality of the system.
Accordingly, every geometry file contains a keyword identifying the physical
dimensionality relevant for scaling.  This should not be confused with the
dimensionality of the computational unit cell; for example, a hydrogen dimer in
a 100 Angstorm bounding box would be considered to be a non-periodic/0D system.

- `0D`:  The geometry is non-periodic
- `1D_{X,Y,Z}`:  The geometry is one-dimensional.  The suffixed {X,Y,Z} denotes
  the axis in which the geometry is periodic.
- `2D_{XY,YZ,XZ}`: The geometry is two-dimensional.  The suffixed {XY,YZ,XZ}
  denotes the plane in which the geometry is periodic.
- `3D`: The geometry is three-dimensional.

**Source Keywords**

Many of the geometry files distributed in this benchmark set were provided to
the ELSI team from outside sources.  To ensure that these sources receive proper
attribution, every geometry file contains keyword(s) indicating how the geometry
was obtained.  Each geometry file which was not generated specifically for this
benchmark set should also include the relevant references and citations in the
Source line of the metadata header.

- `Provided`: The geometry was provided for this benchmark set by an individual
  or research group with permission for redistribution.
- `Generated`: The geometry was created specifically for this benchmark set.
- `Dataset`: The geometry has appeared in another data set.
- `Article`: The geometry has appeared in a published journal article.

**System Type Keywords**

The following keywords indicate the type of physical system that the geometry
is modeling.  These keywords are not mandatory but are useful for
classification.

- `Biological`: The geometry contains a simulation of a biological molecule,
  e.g.  DNA, protein.
- `Liquid`: The geometry contains a simulation of a liquid.
- `Nanosystem`: The geometry contains a simulation of a nanosystem, e.g.
  nanowire, nanotube, nanocrystal,
- `Surface`: The geometry contains a simulation of a surface (including single-
  and few-layer materials).
- `Bulk`: The geometry contains a simulation of a bulk crystal.
