#!/usr/bin/env python
import sys
import numpy as np

if len(sys.argv) < 5:
    print("* The supercell creation script expects four command lines parameters:  the file name and three integers corresponding to the dimensions of the supercell.")
    print("* A different number of parameters was encountered.  Exiting.")
    sys.exit(2)

filename = sys.argv[1]

try:
    theFile = open(filename,"r")
except IOError:
    print("* Could not find geometry.in.unit file, exiting.")
    sys.exit(1)
supercell_dims = [int(i) for i in sys.argv[2:5]]

if any([i <= 0 for i in supercell_dims]):
    print("* You have specified a non-positive dimension for the supercell.  This is not permitted.  Exiting.")
    sys.exit(2)

theFile = theFile.readlines()
header = theFile[0:5]          # The benchmark set contains 5 lines of header containing relevant information about
                               # the phase in question

lattice_vectors = []
atoms = []
species = []
for line in theFile[5:]:
    if len(line.split()) > 0:
        line = line.split()
        if line[0] == "lattice_vector":
            lattice_vectors.append( np.array([float(i) for i in line[1:4]]) )
        elif line[0] == "atom":
            atoms.append( np.array([float(i) for i in line[1:4]]) )
            species.append( line[4] )
        elif line[0] == "atom_frac":
            print("* Direct coordinates have been specified for at least one atom, i.e. the 'atom_frac' keyword is used.")
            print("* Please specify all coordinates in a Cartesian format, i.e. use the 'atom' keyword.  Exiting.")
            sys.exit(3)

if len(lattice_vectors) != 3:
    print("* The supercell creation script expects a geometry.in.unit file with three lattice vectors.  A different number was found.  Exiting.")
    sys.exit(4)

supercell_lattice_vectors = []
for vector in lattice_vectors:
    supercell_lattice_vectors.append( np.array([float(supercell_dims[dim])*vector[dim] for dim in range(3)]) )

print(header[0].strip())
print(header[1].strip())
print(header[2].strip())
print(header[3].strip())
print(header[4].strip())
print("# Supercell dimensions used: %4i %4i %4i" % (supercell_dims[0], supercell_dims[1], supercell_dims[2]))
for vector in supercell_lattice_vectors:
    print("lattice_vector %11.6f %11.6f %11.6f" % (vector[0], vector[1], vector[2]))
for x in range(supercell_dims[0]):
    for y in range(supercell_dims[1]):
        for z in range(supercell_dims[2]):
            for num in range(len(atoms)):
                coords = atoms[num] + x*lattice_vectors[0] + y*lattice_vectors[1] + z*lattice_vectors[2]
                print("atom           %11.6f %11.6f %11.6f  %s" % (coords[0], coords[1], coords[2], species[num]))
